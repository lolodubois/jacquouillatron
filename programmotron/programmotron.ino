//Jaquouillotron v3.0 du 07/06/2019


#include <Servo.h>
#include <SoftwareSerial.h>
#include <DFPlayer_Mini_Mp3.h>

Servo myservo;
SoftwareSerial mySerial(18, 19); // RX, TX
int Inter = 0;
int Inter_old;
int pos = 0;
int pos0 = 0;
int pos1 = 180;
int valdelay = 5;

void setup() {
  //Init servo
  pinMode(8, INPUT);
  myservo.attach(9);
  //Position initiale
  myservo.write(pos0);
  delay(valdelay);
  pos = pos0;

  //Init son
  Serial.begin (9600);
  mySerial.begin (9600);
  mp3_set_serial (mySerial);  //set softwareSerial for DFPlayer-mini mp3 module
  mp3_set_volume (15);
  mp3_play (2);
  delay (1000);

  //Etat initial Inter
  Inter_old = digitalRead(8);
}

void loop() {

  Inter = digitalRead(8);

  if (Inter != Inter_old) {
    Inter_old = Inter;
    if (Inter == 1) {
      mp3_play (3);
      delay (100);
      //delay (3000);
    }
    else {
      mp3_play (4);
      delay (100);
      //delay (3000);
    }
  }

  if (Inter == 0 && pos > pos0) {
    pos -= 1;
    myservo.attach(9);
    myservo.write(pos);
    delay(valdelay);
  }

  if (Inter == 1 && pos < pos1) {
    pos += 1;
    myservo.attach(9);
    myservo.write(pos);
    delay(valdelay);
  }
  if (Inter == 1 && pos >= pos1) {
    myservo.detach();
  }
  if (Inter == 0 && pos <= pos0) {
    myservo.detach();
  }
}
